import { Component, OnInit } from '@angular/core';
import { GlobalVariablesService } from '../services/global-variables.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  itemsCounter: number;
  constructor(public globalService: GlobalVariablesService) {}

  ngOnInit() {
    // change cart items counter globally
    this.globalService.currentCounter.subscribe(counter => this.itemsCounter = counter);
  }
}
