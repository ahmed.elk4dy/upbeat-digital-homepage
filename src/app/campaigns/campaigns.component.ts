import { Component, OnInit } from '@angular/core';
import { GlobalVariablesService } from '../services/global-variables.service';


@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit {
  filteredItems: any = [];
  // this is a hard code for the price and currency value because If i don't have api to make it dynamically and I tried to make it but i faced an issue in event listener on global variable change

  currentCurrency: string = localStorage.getItem('currency');
  dollarPrice: number = 78.60;
  customOptions: any = {
    rtl: true,
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
    nav: true
  };
  sliderContent: any = [
    {
      id: 1,
      type: 'new',
      limit: 120,
      limitPercentage: 50,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'explore'
    },
    {
      id: 2,
      limit: 60,
      limitPercentage: 25,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'explore'
    },
    {
      id: 3,
      limit: 180,
      limitPercentage: 75,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'closing soon'
    },
    {
      id: 4,
      limit: 200,
      limitPercentage: 80,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'closing soon'
    },
    {
      id: 4,
      limit: 240,
      limitPercentage: 100,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'sold out'
    },
    {
      id: 5,
      limit: 240,
      limitPercentage: 100,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'sold out'
    },
    {
      id: 6,
      limit: 120,
      limitPercentage: 50,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'explore'
    },
    {
      id: 7,
      limit: 60,
      limitPercentage: 25,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'explore'
    },
    {
      id: 8,
      limit: 180,
      limitPercentage: 75,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'closing soon'
    },
    {
      id: 9,
      limit: 200,
      limitPercentage: 80,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'closing soon'
    },
    {
      id: 10,
      limit: 240,
      limitPercentage: 100,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'sold out'
    },
    {
      id: 11,
      limit: 240,
      limitPercentage: 100,
      name: 'grey jacket',
      price: 25.60,
      currency: 'aed',
      liked: false,
      state: 'sold out'
    }
  ];
  soldOutItemsLoaded: boolean;
  itemsCounter: number;

  constructor(public globalVariables: GlobalVariablesService) { }

  ngOnInit() {
    // reset filtered items array
    this.filteredItems = this.sliderContent;
    // Listen for the global change for cart items counter
    this.globalVariables.currentCounter.subscribe(counter => this.itemsCounter = counter);
    // Listen for the global change for the app currency
    this.globalVariables.currentCurrency.subscribe(currency => this.currentCurrency = currency);
    localStorage.setItem('currency', 'aed');
  }

  // add color to the limit bar depending on the limit
  limitClass(limit: number) {
    if (limit > 120) {
      return 'progress-bar bg-danger';
    } else if (limit < 120) {
      return 'progress-bar bg-warning';
    } else if (limit === 120) {
      return 'progress-bar bg-info';
    }
  }

  // make love button toggle on click
  checkLike(id) {
    this.sliderContent.forEach(slide => {
      if (id === slide.id) {
        slide.liked = !slide.liked;
      }
    });
  }
  addLike(like) {
    if (like === true) {
      return 'fa fa-heart liked';
    } else if (like === false) {
      return 'fa fa-heart unliked';
    }
  }

  // filter products regarding to closing soon, explore and sold out and adding overlay on sold out itemss
  filterProducts(flag: string) {
    this.filteredItems = this.sliderContent;
    document.getElementById('selectedFilter').innerHTML = '';
    document.getElementById('slectedFilteritemsCount').innerHTML = '';
    if (flag === 'closing soon') {
      this.filteredItems = this.sliderContent.filter(item => {
        if (item.state === 'closing soon') {
          this.soldOutItemsLoaded = false;
          document.getElementById('selectedFilter').innerHTML = item.state;
          return item;
        }
      });
      document.getElementById('slectedFilteritemsCount').innerHTML = '(' + this.filteredItems.length + ' Campaigns)';
    } else if (flag === 'explore') {
      this.filteredItems = this.sliderContent.filter(item => {
        if (item.state === 'explore') {
          this.soldOutItemsLoaded = false;
          document.getElementById('selectedFilter').innerHTML = item.state;
          return item;
        }
      });
      document.getElementById('slectedFilteritemsCount').innerHTML = '(' + this.filteredItems.length + ' Campaigns)';
    } else if (flag === 'sold out') {
      this.filteredItems = this.sliderContent.filter(item => {
        if (item.state === 'sold out') {
          this.soldOutItemsLoaded = true;
          document.getElementById('selectedFilter').innerHTML = item.state;
          return item;
        }
      });
      document.getElementById('slectedFilteritemsCount').innerHTML = '(' + this.filteredItems.length + ' Campaigns)';
    }
  }

  // Increase cart counter
  addToCart() {
   this.itemsCounter++;
   this.globalVariables.changeCounter(this.itemsCounter);
  }

}
