import { Component } from '@angular/core';
import { GlobalVariablesService } from './services/global-variables.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'upbeat-homepage';
  appDir: string;

  constructor(public globalService: GlobalVariablesService) { }

  languageChangeHandler(e: any) {
    const dir = e.target.value;
    if (dir === 'eng') {
      this.appDir = 'ltr';
      this.globalService.changeDirection(this.appDir);
    } else if (dir === 'arb') {
      this.appDir = 'rtl';
      this.globalService.changeDirection(this.appDir);
    }
  }

  currencyChangeHandler(e: any) {
    const currentCurrency = e.target.value;
    this.globalService.changeCurrency(currentCurrency);
  }
}
