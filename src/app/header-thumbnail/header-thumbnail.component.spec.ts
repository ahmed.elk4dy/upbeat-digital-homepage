import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderThumbnailComponent } from './header-thumbnail.component';

describe('HeaderThumbnailComponent', () => {
  let component: HeaderThumbnailComponent;
  let fixture: ComponentFixture<HeaderThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
