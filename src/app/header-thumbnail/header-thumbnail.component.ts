import { Component, OnInit } from '@angular/core';
import { GlobalVariablesService } from '../services/global-variables.service';

@Component({
  selector: 'app-header-thumbnail',
  templateUrl: './header-thumbnail.component.html',
  styleUrls: ['./header-thumbnail.component.scss']
})
export class HeaderThumbnailComponent implements OnInit {
  carouselDir: boolean;
  customOptions: any = {
    rtl: true,
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: true
  };

  constructor(public globalService: GlobalVariablesService) {
    let direction: string;
    this.globalService.currentDir.subscribe(dir => {
      direction = dir;
      if (direction === 'ltr')
        this.carouselDir === false;
      if (direction === 'rtl')
        this.carouselDir === true;
    });
  }

  ngOnInit() {
  }

}
