import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GlobalVariablesService {
  // Global variable for cart items counter
  private counter = new BehaviorSubject(1);
  currentCounter = this.counter.asObservable();
  // Global variable for change app direction
  private direction = new BehaviorSubject('ltr');
  currentDir = this.direction.asObservable();
  // Global variable for currency
  private currency = new BehaviorSubject('aed');
  currentCurrency = this.currency.asObservable();

  constructor() { }

  // Global function for change cart items counter globaly
  changeCounter(count: number) {
    this.counter.next(count);
  }

  // Global function for change app directionglobaly
  changeDirection(dir: string) {
    this.direction.next(dir);
  }

  // Global function for change the Currency
  changeCurrency(currency: string) {
    this.currency.next(currency);
    localStorage.setItem('currency', currency);
  }

}
